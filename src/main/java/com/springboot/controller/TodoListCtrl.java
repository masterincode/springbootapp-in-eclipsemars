package com.springboot.controller;


import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.dto.TodoListDto;
import com.springboot.model.TodoModel;
import com.springboot.response.ResponseObject;
import com.springboot.service.TodoListService;

@Component
// @CrossOrigin(origins="http://localhost:4200")
@RestController
public class TodoListCtrl {
	@Autowired
	private TodoListService todoListService;

	HashMap<String, Object> object = new HashMap<String, Object>();

	@RequestMapping("/fetchTodoList")
	public ResponseObject fetchTodoList(@RequestBody TodoModel model) {
		System.out.println("--in ctrl--fetchTodoList-" + model);
		return todoListService.findByUsername(model);
	}

	@RequestMapping("/submitToDoList")
	public HashMap<String, Object> submitToDoList(@RequestBody TodoListDto dto) {

		System.out.println("-submitToDoList  ctrl---" + dto.toString());
		try {
			todoListService.saveData(dto);
			object.put("result", "SUCCESS");

		} catch (Exception e) {
			object.put("result", "FAILURE");
		}
		return object;
	}

	@RequestMapping("/deleteData")
	public void deleteData(@RequestBody Long id) {
		System.out.println("-submitToDoList  ctrl---" + id);
		todoListService.deleteData(id);
	}

	public HashMap<String, Object> getObject() {
		return object;
	}

}
