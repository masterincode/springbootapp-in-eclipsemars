package com.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.model.HelloWorldModel;
import com.springboot.model.RegisterationModel;
import com.springboot.response.ResponseObject;

@RestController
public class HardCodeDemoCtrl
{
	@Autowired
	private ResponseObject response;
		
	@GetMapping(path="/getString")
	public String helloworld()
	{
		return "HELLO WORLD"; 
	}
	
	
	@GetMapping(path="/getObject")
	public HelloWorldModel getHelloWorldObj()
	{
		return new HelloWorldModel("Balmukund Singh", "28", "Mumbai");
	}
	
	@GetMapping(path="/getListObject")
	public ResponseObject getListHelloWorldObj()
	{
		List<HelloWorldModel> list=new ArrayList<HelloWorldModel>();
		list.add(new HelloWorldModel("Balmukund Singh", "28", "Mumbai"));
		list.add(new HelloWorldModel("Ankit", "30", "Navi Mumbai"));
		list.add(new HelloWorldModel("Raju", "20", "Nashik"));
		response.addData("studentsDetails", list);
		response.addData("response","SUCCESS");
		return response;
	}
	
	@RequestMapping("/fetchData")
	public RegisterationModel fetchData()
	{
		return new RegisterationModel("bms241093" ,"Asdf@1234","Balmukund Singh", "28" ,"Mumbai" );
	}
	
	@PostMapping("/submitData")
	public ResponseObject submitData(@RequestBody RegisterationModel registerationModel)
	{
		System.out.println("--submit data---"+registerationModel.toString());
		response.addData("response","SUCCESS");
		return response;
	}
	
	
	
	
	
	
}
