package com.springboot.service;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.dto.TodoListDto;
import com.springboot.model.TodoModel;
import com.springboot.repository.TodoListRepo;
import com.springboot.response.ResponseObject;

@Service
public class TodoListService {
	@Autowired
	private TodoListRepo todoListRepo;

	@Autowired
	private ResponseObject response;

	@Transactional
	public ResponseObject findByUsername(TodoModel model) {
		System.out.println("---service data--" + todoListRepo.findByUsername(model.getUsername()));
		response.addData("fetchTodoList", todoListRepo.findByUsername(model.getUsername()));
		return response;
	}

	@Transactional
	public void saveData(TodoListDto dto) {
		System.out.println("--while saving--" + dto.toString());
		todoListRepo.save(dto);
	}

	public void deleteData(Long id) {
		todoListRepo.deleteById(id);
	}

}
