package com.springboot.service;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.springboot.dto.CityMasterDto;
import com.springboot.repository.CityMasterRepo;
import com.springboot.response.ResponseObject;

@Service
public class CityMasterService
{
	@Autowired
	private CityMasterRepo dataTableRepo;
	
	@Autowired
	private ResponseObject response;
	
	
	public ResponseObject findCity(Long id) 
	{
		response.addData("cityById", dataTableRepo.findById(id)); 
		return response;
	}
	
	public ResponseObject findAll()
	{
		Iterable<CityMasterDto> iterable =dataTableRepo.findAll();
		Iterator<CityMasterDto> itr = iterable.iterator();
		List<CityMasterDto> list = new ArrayList<>();
		for(CityMasterDto d:iterable)
		{
			System.out.println(d);
			list.add(d);
		}
		
		Comparator<CityMasterDto> comp=(e1,e2)->(e1.getId()>e2.getId())?-1:(e1.getId()>e2.getId())?1:0;
 		List<CityMasterDto> list2=list.stream().sorted(comp).collect(Collectors.toList());
		
//		while(itr.hasNext())
//		{
//			System.out.println(itr.next());
//		}
		
		response.addData("cityById", list2);
		return response;
	}
	
	public MappingJacksonValue findAllDistrict()
	{
		// Dynamic Filtering of response
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("district");
		FilterProvider filters = new SimpleFilterProvider().addFilter("ThisIsDtoKey", filter);   // Kindly enable the @JsonFilter("ThisIsDtoKey") in DataTableDto
		MappingJacksonValue mapping = new MappingJacksonValue(dataTableRepo.findAll());
		mapping.setFilters(filters);
//		response.addData("cityById", mapping);
		return mapping;
	}
	
	
	
	
	public ResponseObject save(CityMasterDto dto)
	{
		response.addData("SUCCESS", dataTableRepo.save(dto));
		return response;
	}
	
}
