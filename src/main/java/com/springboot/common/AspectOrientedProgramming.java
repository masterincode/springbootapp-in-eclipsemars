package com.springboot.common;

import java.util.Arrays;
import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/*
 * In order to implement AOP u should have three jars
 * spring-aop-5.1.5.jar
 * spring-aspects-x.x.x.jar
 * aspectjweaver-x.x.x.jar
 * 
 * */


@Aspect
@Component
public class AspectOrientedProgramming {
	
	/* In case if it is not working kindly add @EnableAspectJAutoProxy(proxyTargetClass=true) in RestfulWebServicesApplication main class */

	@Before("execution(* com.springboot.service.TodoListService.*(..))")
	public void before(JoinPoint joinPoint){
		System.out.println("Before----------"+joinPoint.getSignature().getName());
		System.out.println(joinPoint.getSignature().getName());
		System.out.println(Arrays.toString(joinPoint.getArgs()));
	}
	
	@AfterReturning(pointcut="execution(* com.springboot.service.TodoListService.*(..))",returning="result")
	public void after(JoinPoint joinPoint,Object result ){
		System.out.println("after-----------------"+joinPoint.getSignature().getName());
		System.out.println(joinPoint.getSignature().getName());
		System.out.println("result =="+result);
		
	}
	
	
//	@Around(value="execution(* org.rest.webservices.service.HelloWorldService.*(..))")
//	public void around(ProceedingJoinPoint joinPoint) throws Throwable{
//		
//		long startTime = new Date().getTime();
//		Object result = joinPoint.proceed();
//		long endTime = new Date().getTime();
//		System.out.println("Execution Time-------------------------------"+(endTime-startTime));
//	}
	
	
}
