package com.springboot.common;


import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.springboot.common.exception.ExceptionResponse;
import com.springboot.response.PaasingArguments;


@ControllerAdvice // this mean it is applicable for across all controller
public class MyGlobalExceptionHandler extends ResponseEntityExceptionHandler {
	 
	
	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,HttpHeaders headers, HttpStatus status, WebRequest request)
	{
		ExceptionResponse exceptionResponse=null;
		PaasingArguments arguments=null;
		 
		List<ExceptionResponse> list = new ArrayList<>();
		for(int i=0;i<ex.getBindingResult().getFieldErrors().size();i++)
		{
			exceptionResponse = new ExceptionResponse();
//			exceptionResponse.setFieldErrorList(ex.getBindingResult().getFieldErrors());
			exceptionResponse.setDefaultMessage(ex.getBindingResult().getFieldErrors().get(i).getDefaultMessage());
			exceptionResponse.setDtoName(ex.getBindingResult().getFieldErrors().get(i).getObjectName());
			exceptionResponse.setField(ex.getBindingResult().getFieldErrors().get(i).getField());
			exceptionResponse.setValidationtype(ex.getBindingResult().getFieldErrors().get(i).getCode());
			exceptionResponse.setMessage("Validation Error");
			if(ex.getBindingResult().getFieldErrors().get(i).getArguments().length>1)
			{
				arguments = new PaasingArguments();
				arguments.setMinlength(ex.getBindingResult().getFieldErrors().get(i).getArguments()[2]); 
				arguments.setMaxlength(ex.getBindingResult().getFieldErrors().get(i).getArguments()[1]);
				exceptionResponse.setArguments(arguments);
			}
			
			
			list.add(exceptionResponse);
		}
		return new ResponseEntity<Object>(list,status);
	}
	
	
	

}
