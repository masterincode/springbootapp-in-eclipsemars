package com.springboot.common.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.FieldError;

import com.springboot.response.PaasingArguments;

public class ExceptionResponse {

	private String message;
	private String defaultMessage;
	private String dtoName;
	private String field;
	private boolean bindingFailure;
	private String validationtype;

	private PaasingArguments arguments;

	private List<FieldError> fieldErrorList;


	public ExceptionResponse() {
		super();

		arguments = new PaasingArguments();
		fieldErrorList = new ArrayList<FieldError>();

	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public boolean isBindingFailure() {
		return bindingFailure;
	}

	public void setBindingFailure(boolean bindingFailure) {
		this.bindingFailure = bindingFailure;
	}

	public String getValidationtype() {
		return validationtype;
	}

	public void setValidationtype(String validationtype) {
		this.validationtype = validationtype;
	}

	public PaasingArguments getArguments() {
		return arguments;
	}

	public void setArguments(PaasingArguments arguments) {
		this.arguments = arguments;
	}

	public List<FieldError> getFieldErrorList() {
		return fieldErrorList;
	}

	public void setFieldErrorList(List<FieldError> fieldErrorList) {
		this.fieldErrorList = fieldErrorList;
	}

	public String getDtoName() {
		return dtoName;
	}

	public void setDtoName(String dtoName) {
		this.dtoName = dtoName;
	}

}
