package com.springboot.common;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CityNotFoundException extends Exception {
	
    private static final long serialVersionUID = 1680984909848120438L;

	public CityNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

		
}
