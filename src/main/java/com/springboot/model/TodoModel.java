package com.springboot.model;
public class TodoModel {

	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "TodoModel [username=" + username + "]";
	}
	
	
}
