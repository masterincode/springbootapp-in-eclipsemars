package com.springboot.model;

public class RegisterationModel
{
	//Note: 
//	Body of request 
//	{
//	    "username": "bms241093",
//	    "password": "Asdf@1234",
//	    "name": "Balmukund Singh",
//	    "age": "28",
//	    "city": "Mumbai"
//	}
	
//	select JSON(application/json)
	
//	Make default constructor manadatory
	public RegisterationModel() {
		super();
		
	}
	 
	private String username;
	private String password;
	private String name;
	private String age;
	private String city;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "RegisterationModel [username=" + username + ", password=" + password + ", name=" + name + ", age=" + age
				+ ", city=" + city + "]";
	}
	public RegisterationModel(String username, String password, String name, String age, String city) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.age = age;
		this.city = city;
	}

}
