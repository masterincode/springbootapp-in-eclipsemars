package com.springboot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springboot.dto.CityMasterDto;

@Repository
public interface CityMasterRepo extends CrudRepository<CityMasterDto,Long> {

	Object findById(Long id); 
	
}
