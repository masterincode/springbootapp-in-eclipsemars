package com.springboot.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.springboot.dto.TodoListDto;

@Repository
public interface TodoListRepo extends JpaRepository<TodoListDto,Long>
{
	@Query(value = "select * from todo_list where username = ?1", nativeQuery = true)
	List<TodoListDto> findByUsername(String username);

	void deleteById(Long id);

}
