package com.springboot.dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="City_Master")
public class CityMasterDto
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;

	@Column(name="NAME")
	@Size(max=8 ,message="Name should be 3 Charcters")
	private String name;
	
	
    @Size(max=3 ,message="Country Code should be 3 Charcters") 
	@Column(name="COUNTRYCODE")
	private String countryCode;
	
    @NotNull(message="District is mandatory")
	@Column(name="DISTRICT")
	private String district;
	
	@Column(name="POPULATION")
	@JsonIgnore // to filter column
	private String population;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getPopulation() {
		return population;
	}
	public void setPopulation(String population) {
		this.population = population;
	}
	
	@Override
	public String toString() {
		return "DataTableDto [id=" + id + ", name=" + name + ", countryCode=" + countryCode + ", district=" + district
				+ ", population=" + population + "]";
	}
	public CityMasterDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public CityMasterDto(Long id, String name, String countryCode, String district, String population) {
		super();
		this.id = id;
		this.name = name;
		this.countryCode = countryCode;
		this.district = district;
		this.population = population;
	}
}
